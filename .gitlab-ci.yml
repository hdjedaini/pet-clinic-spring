stages:
  - build
  - test
  - quality
  - package
  - deploy
  - performance
  - security

cache:
    paths:
    - .m2/repository
    key: "$CI_JOB_NAME"

build_job:
  stage: build
  image: eclipse-temurin:17-jdk-alpine
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true

test_job:
  stage: test
  image: eclipse-temurin:17-jdk-alpine
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true

code_quality_job:
  stage: quality
  allow_failure: true
  image: docker:stable
  services:
    - docker:dind
  script:
    - mkdir codequality-results
    - docker run
        --env CODECLIMATE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        --volume /tmp/cc:/tmp/cc
        codeclimate/codeclimate analyze -f html > ./codequality-results/index.html
  artifacts:
    paths:
      - codequality-results/

package_job:
  stage: package
  image: eclipse-temurin:17-jdk-alpine
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375 
    PWD: ip172-19-0-27-cihn84ksnmng0085ajvg 
  script:
    - apk add --no-cache docker-cli-compose
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - ./mvnw install -PbuildDocker -DskipTests=true -DpushImage
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
    - docker compose build
    - docker compose push

deploy_staging_job:
  stage: deploy
  image: alpine
  script:
    - apk add --no-cache docker-cli-compose
    - export DOCKER_HOST=tcp://ip172-19-0-27-cihn84ksnmng0085ajvg.direct.labs.play-with-docker.com:2375
    - docker compose down
    - docker compose up -d
  environment:
    name: staging
    url: http://ip172-19-0-27-cihn84ksnmng0085ajvg-8080.direct.labs.play-with-docker.com

performance_job:
  stage: performance
  image: alpine
  services:
    - docker:dind
  variables:
    URL: http://$PWD-8080.direct.labs.play-with-docker.com/
    DOCKER_HOST: tcp://docker:2375
  script:
    - apk add --no-cache curl docker-cli
    - x=1; while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://$PWD-8080.direct.labs.play-with-docker.com/)" != "200" || $x -le 60 ]]; do sleep 5; echo $(( x++ )); done || false
    - mkdir gitlab-exporter
    - wget -O ./gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/master/index.js
    - mkdir sitespeed-results
    - docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io --plugins.add ./gitlab-exporter --outputFolder sitespeed-results $URL
    - mv sitespeed-results/data/performance.json performance.json
  artifacts:
    paths:
      - sitespeed-results/
    reports:
      performance: performance.json

container_scanning:
  stage: security
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
    FULL_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    DOCKER_HOST: tcp://docker:2375
  script:
    - trivy image --clear-cache
    - trivy image --exit-code 0 --cache-dir .trivycache/ --no-progress --format template --template "@/contrib/gitlab.tpl"
        --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$FULL_IMAGE_NAME"
    - trivy image --exit-code 0 --cache-dir .trivycache/ --no-progress "$FULL_IMAGE_NAME"
    - trivy image --exit-code 1 --cache-dir .trivycache/ --severity CRITICAL --no-progress "$FULL_IMAGE_NAME"
  cache:
    paths:
      - .trivycache/
  artifacts:
    when: always
    reports:
      container_scanning: gl-container-scanning-report.json